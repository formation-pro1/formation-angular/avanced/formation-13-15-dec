import {Injectable} from '@angular/core';
import {Actions, concatLatestFrom, createEffect, ofType} from '@ngrx/effects';
import {OrdersService} from '../../orders/services/orders.service';
import {
  createOrder, createOrderFailure,
  createOrderSuccess,
  loadOrders,
  loadOrdersFailure,
  loadOrdersSuccess, saveOrder, saveOrderFailure, saveOrderSuccess
} from '../actions/order.actions';
import {catchError, combineLatest, map, mergeMap, switchMap, tap} from 'rxjs/operators';
import {Order} from '../../core/models/order';
import {of} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AppState} from '../index';
import {selectOrdersList} from '../selectors/order.selectors';
import {Router} from '@angular/router';


@Injectable()
export class OrderEffects {

  loadOrders$ = createEffect(
    () => this.actions$.pipe(
      ofType(loadOrders),
      switchMap(() => this.orderService.orders),
      map((orders: Order[]) => loadOrdersSuccess({orders})),
      catchError(err => of(loadOrdersFailure({error: err})))
    )
  );

  // getOrders$ = createEffect(
  //   () => this.actions$.pipe(
  //     ofType(loadOrders),
  //     concatLatestFrom(() => this.store.pipe(select(selectOrdersList))),
  //     switchMap(([_, orders]) => {
  //       if (orders !== null && orders.length > 0) {
  //         return of(orders);
  //       }
  //       return this.orderService.orders;
  //     }),
  //     map((orders: Order[]) => loadOrdersSuccess({orders})),
  //     catchError(err => of(loadOrdersFailure({error: err})))
  //   )
  // );

  createOrder$ = createEffect(
    () => this.actions$.pipe(
      ofType(createOrder),
      switchMap((action) => this.orderService.add$(action.order)),
      map((order: Order) => createOrderSuccess({order})),
      catchError(err => of(createOrderFailure({error: err})))
    )
  );


  saveOrder$ = createEffect(
    () => this.actions$.pipe(
      ofType(saveOrder),
      mergeMap(({payload}) => this.orderService.update$(payload)),
      map(_ => saveOrderSuccess()),
      catchError(err => of(saveOrderFailure({error: err})))
    )
  );

  // Centraliser le comportement de succès
  updateOrCreateOrderSuccess$ = createEffect(
    () => this.actions$.pipe(
      ofType(saveOrderSuccess, createOrderSuccess),
      tap(() => this.router.navigate(['orders']))
    ),
    {
      dispatch: false
    }
  )

  constructor(private actions$: Actions,
              private store: Store<AppState>,
              private router: Router,
              private orderService: OrdersService) {
  }

}
