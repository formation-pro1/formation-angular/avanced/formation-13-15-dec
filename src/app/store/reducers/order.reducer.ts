import {Action, createReducer, on} from '@ngrx/store';
import {Order} from '../../core/models/order';
import * as OrderActions from '../actions/order.actions';


export const orderFeatureKey = 'order';

export interface State {
  orders: Order[];
  loaded: boolean;
  loading: boolean;
  selectedOrder: Order | null;
  errors: string[];
}

export const initialState: State = {
  orders: [],
  loaded: false,
  loading: false,
  errors: [],
  selectedOrder: null
};

export const reducer = createReducer(
  initialState,
  on(OrderActions.loadOrders, state => ({...state, loading: true})),
  on(OrderActions.loadOrdersSuccess, (state, {orders}) => ({...state, loading: false, loaded: true, orders: orders})),
  on(OrderActions.loadOrdersFailure, (state, {error}) => ({
    ...state,
    loading: false,
    loaded: false,
    errors: [...state.errors, error.toString()]
  })),


  on(OrderActions.createOrder, (state) => ({...state, loading: true})),
  on(OrderActions.createOrderSuccess, (state, {order}) => ({
    ...state,
    loading: false,
    loaded: true,
    orders: [...state.orders, order]
  })),
  on(OrderActions.createOrderFailure, (state, {error}) => ({
    ...state,
    loading: false,
    loaded: false,
    errors: [...state.errors, error.toString()]
  })),


  on(OrderActions.selectOrder, (state, {payload}) => ({...state, selectedOrder: payload})),
  on(OrderActions.saveOrder, (state) => ({...state, loading: true})),
  on(OrderActions.saveOrderSuccess, (state) => ({
    ...state,
    selectedOrder: null,
    loaded: true,
    loading: false
  })),
  on(OrderActions.saveOrderFailure, (state, {error}) => ({
    ...state,
    loading: false,
    loaded: false,
    errors: [...state.errors, error.toString()]
  }))
);
