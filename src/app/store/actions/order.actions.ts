import {createAction, props} from '@ngrx/store';
import {Order} from '../../core/models/order';

export const loadOrders = createAction(
  '[Order] Load Orders'
);

export const loadOrdersSuccess = createAction(
  '[Order] Load Orders Success',
  props<{
    orders: Order[]
  }>()
);

export const loadOrdersFailure = createAction(
  '[Order] Load Orders Failure',
  props<{
    error: any
  }>()
);

export const createOrder = createAction(
  '[Order] Create Order',
  props<{
    order: Order
  }>()
);

export const createOrderSuccess = createAction(
  '[Order] Create Order Success',
  props<{
    order: Order
  }>()
);

export const createOrderFailure = createAction(
  '[Order] Create Order Failure',
  props<{
    error: any
  }>()
);

export const selectOrder = createAction(
  '[Order] Select Order',
  props<{
    payload: Order
  }>()
);

export const saveOrder = createAction(
  '[Order] Save Order',
  props<{
    payload: Order
  }>()
);

export const saveOrderSuccess = createAction(
  '[Order] Save Order Success'
);

export const saveOrderFailure = createAction(
  '[Order] Save Order Failure',
  props<{
    error: any
  }>()
);







