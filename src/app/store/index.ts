import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector, INIT,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromOrder from './reducers/order.reducer';


export interface AppState {
  [fromOrder.orderFeatureKey]: fromOrder.State;
}

export const reducers: ActionReducerMap<AppState> = {

  [fromOrder.orderFeatureKey]: fromOrder.reducer,
};

export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => {
    console.log('state', state);
    console.log('action', action);
    return reducer(state, action);
  }
};

export function cacheAppState(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => {
    let reducedState = reducer(state, action);
    if (action.type === INIT) {
      const dataInStorage = window.localStorage.getItem('state');
      if (dataInStorage) {
        reducedState = JSON.parse(dataInStorage);
      }
    } else {
      window.localStorage.setItem('state', JSON.stringify(reducedState));
    }

    return reducedState;
  }
};

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [debug, cacheAppState] : [];
