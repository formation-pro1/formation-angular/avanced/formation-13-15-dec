import { createSelector } from '@ngrx/store';
import {AppState} from '../index';
import * as fromOrder from '../reducers/order.reducer';

export const selectFeature = (state: AppState) => state[fromOrder.orderFeatureKey];

export const selectOrdersList = createSelector(
  selectFeature,
  (state: fromOrder.State) => state.orders
);

export const selectOrdersLoading = createSelector(
  selectFeature,
  (state: fromOrder.State) => state.loading
);

export const selectSelectedOrder = createSelector(
  selectFeature,
  (state: fromOrder.State) => state.selectedOrder
);
