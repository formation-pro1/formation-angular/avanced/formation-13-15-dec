import {Directive, Injectable, ViewChild} from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {PageAddOrderComponent} from '../../orders/pages/page-add-order/page-add-order.component';
import {PageEditOrderComponent} from '../../orders/pages/page-edit-order/page-edit-order.component';

export abstract class HasFormDirty {
  abstract isFormDirty(): boolean
}

@Directive()
export abstract class CheckForm<T extends HasFormDirty> {

  @ViewChild('form')
  private formComponent!: T;

  public isFormDirty(): boolean {
    return this.formComponent.isFormDirty();
  }
}

@Injectable({
  providedIn: 'root'
})
export class CheckFormGuard implements CanDeactivate<CheckForm<any>> {
  canDeactivate(
    component: CheckForm<any>,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    // Creer la logique pour quitter le composant d'édition ou de création
    if (component.isFormDirty()) {
      return confirm("Voulez vous réellement quitter la page ?");
    }
    return true;
  }

}
