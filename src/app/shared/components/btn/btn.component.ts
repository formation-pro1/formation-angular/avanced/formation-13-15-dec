import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-btn',
  templateUrl: './btn.component.html',
  styleUrls: ['./btn.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BtnComponent implements OnInit {

  @Input()
  public label!: string;

  @Input()
  public route!: string;

  constructor() { }

  ngOnInit(): void {
  }
}
