import { Component } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Voici la formation du lundi 28';

  // COLD OBSERVABLE = NETFLIX
  private obs1 = new Observable((data) => {
    data.next(Math.random())
  });

  // HOT OBSERVABLE = TV
  private subject = new Subject();
  private behaviorSubject = new BehaviorSubject('valeur initiale');

  constructor() {

    // Observable
    // RxJs = Reactive JS
    this.obs1.subscribe(
      (next) => {console.log('obs1: ', next)}
    );
    this.obs1.subscribe(
      (next) => {console.log('obs1: ', next)}
    );


    // Subject
    this.subject.next('valeur Subject 1');
    this.subject.next('valeur Subject 2');
    this.subject.subscribe(
      (next) => {
        console.log('personne 1', next)
      }
    );

    this.subject.subscribe(
      (next) => {
        console.log('personne 2', next)
      }
    );

    this.subject.next('valeur Subject 3');

    // BehaviorSubject

    this.behaviorSubject.subscribe(
      (next) => { console.log('BS personne 1', next)}
    );

    this.behaviorSubject.next('valeur 1');
    this.behaviorSubject.next('valeur 2');
    this.behaviorSubject.next('valeur 3');

    this.behaviorSubject.subscribe(
      (next) => { console.log('BS personne 2', next)}
    );

  }
}
