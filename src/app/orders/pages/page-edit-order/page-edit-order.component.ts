import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Order} from 'src/app/core/models/order';
import {OrdersService} from '../../services/orders.service';
import {CheckForm} from '../../../shared/guards/check-form.guard';
import {FormOrderComponent} from '../../components/form-order/form-order.component';
import {map, switchMap} from 'rxjs/operators';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../store';
import {selectSelectedOrder} from '../../../store/selectors/order.selectors';
import {saveOrder} from '../../../store/actions/order.actions';

@Component({
  selector: 'app-page-edit-order',
  templateUrl: './page-edit-order.component.html',
  styleUrls: ['./page-edit-order.component.scss']
})
export class PageEditOrderComponent extends CheckForm<FormOrderComponent> implements OnInit {

  public order$!: Observable<Order | null>;

  constructor(private store: Store<AppState>,
              private router: Router,
              private ordersService: OrdersService) {
    super();
  }

  ngOnInit(): void {
    // this.order$ = this.activatedRoute.paramMap.pipe(
    //   switchMap((data) => this.ordersService.getOrderById$(Number(data.get('id'))))
    // );


    this.order$ = this.store.pipe(select(selectSelectedOrder));
  }

  public updateOrder(order: Order): void {
    this.store.dispatch((saveOrder({payload: order})));
  }

}
