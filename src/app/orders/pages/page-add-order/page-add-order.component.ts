import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Order} from 'src/app/core/models/order';
import {OrdersService} from '../../services/orders.service';
import {FormOrderComponent} from '../../components/form-order/form-order.component';
import {CheckForm} from '../../../shared/guards/check-form.guard';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store';
import {createOrder} from '../../../store/actions/order.actions';
import {fromEvent} from 'rxjs';
import {concatMap, delay, exhaustMap, mergeMap, switchMap, tap} from 'rxjs/operators';

@Component({
  selector: 'app-page-add-order',
  templateUrl: './page-add-order.component.html',
  styleUrls: ['./page-add-order.component.scss'],
})
export class PageAddOrderComponent extends CheckForm<FormOrderComponent> implements OnInit, AfterViewInit {

  public item = new Order();

  @ViewChild('test')
  private saveButton!: ElementRef;

  constructor(private store: Store<AppState>,
              private orderService: OrdersService,
              private router: Router) {
    super();
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {


    fromEvent(this.saveButton.nativeElement, 'click')
      .pipe(
        tap(value => console.log(value)),
        concatMap(() => this.orderService.add$(new Order())),
        tap(value => console.log(value))
      )
      .subscribe();

    // switchMap -> Annule la requête en cours si un nouveau flux est tranmis
    // exhaustMap -> Attend que la requête en cours se termine avant d'accepter un nouveau flux
    // concatMap -> Ajoute à la liste des requêtes à exécuter en préservant l'ordre
    // mergeMap -> Exécute en parallèle l'ensemble des  requêtes


  }

  public saveOrder(order: Order): void {
    this.store.dispatch(createOrder({order}));
    this.router.navigate(['orders']);
  }
}
