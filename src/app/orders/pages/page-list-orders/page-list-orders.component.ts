import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {interval, Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {StateOrder} from 'src/app/core/enums/state-order';
import {Order} from 'src/app/core/models/order';
import {OrdersService} from '../../services/orders.service';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../store';
import {selectOrdersList, selectOrdersLoading} from '../../../store/selectors/order.selectors';
import {loadOrders, selectOrder} from '../../../store/actions/order.actions';

@Component({
  selector: 'app-page-list-orders',
  templateUrl: './page-list-orders.component.html',
  styleUrls: ['./page-list-orders.component.scss'],
})
export class PageListOrdersComponent implements OnInit {

  public ordersCollection$!: Observable<Order[]>;
  public possibleStates = Object.values(StateOrder);

  public isLoading$!: Observable<boolean>;

  public headers = [
    'Action',
    'Type',
    'Client',
    'NbJours',
    'Tjm HT',
    'Total HT',
    'Total TTC',
    'State',
  ];

  public myTitle = 'Liste des commandes';

  constructor(private store: Store<AppState>,
              private ordersService: OrdersService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {

    this.store.dispatch(loadOrders());

    this.isLoading$ = this.store.pipe(select(selectOrdersLoading));

    this.ordersCollection$ = this.store
      .pipe(
        select(selectOrdersList),
        // Manipulation métier
        map((tableau: Order[]) => {
          return tableau.map((element: Order) => {
            return new Order(element);
          })
        }),
      );
  }

  ngOnInit(): void {
  }

  public changeTitle(): void {
    this.myTitle = 'Liste des commandes ' + Math.random();
  }

  public changeState(item: Order, selectEvent: any): void {
    this.ordersService.changeState(item, selectEvent.target.value).subscribe(
      (res) => {
        console.log(res);
      }
    );
  }

  public navigateToEdit(order: Order): void {
    this.store.dispatch(selectOrder({payload: order}));
    this.router.navigate(['orders', 'edit', order.id]);
  }

  public refreshOrders(): void {
    this.store.dispatch(loadOrders());
  }
}
