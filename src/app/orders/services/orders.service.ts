import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {StateOrder} from 'src/app/core/enums/state-order';
import {Order} from 'src/app/core/models/order';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  private orders$!: Observable<Order[]>;
  private urlApi = environment.urlApi;

  constructor(private http: HttpClient) {
    this.orders = this.http.get<Order[]>(`${this.urlApi}/orders`);
  }

  // get collection
  public get orders(): Observable<Order[]> {
    return this.orders$;
  }

  // set collection
  public set orders(col: Observable<Order[]>) {
    this.orders$ = col;
  }

  // change state item
  public changeState(item: Order, state: StateOrder): Observable<Order> {
    item.state = state;
    return this.update$(item);
  }

  // update item in collection
  public update$(item: Order): Observable<Order> {
    return this.http.put<Order>(`${this.urlApi}/orders/${item.id}`, item);
  }

  // add item
  public add$(item: Order): Observable<Order> {
    return this.http.post<Order>(`${this.urlApi}/orders`, item);
  }

  // delete item
  public delete$(item: Order): Observable<Order> {
    return this.http.delete<Order>(`${this.urlApi}/orders/${item.id}`);
  }

  // get item by id
  public getOrderById$(id: number): Observable<Order> {
    return this.http.get<Order>(`${this.urlApi}/orders/${id}`);
  }
}
