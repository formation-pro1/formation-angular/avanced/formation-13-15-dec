import { TestBed } from '@angular/core/testing';

import { GetOrdersResolver } from './get-orders.resolver';

describe('GetOrdersResolver', () => {
  let resolver: GetOrdersResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetOrdersResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
