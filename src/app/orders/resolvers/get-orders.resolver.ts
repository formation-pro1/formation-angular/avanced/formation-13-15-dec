import {Injectable} from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {Observable, of} from 'rxjs';
import {OrdersService} from '../services/orders.service';
import {Order} from '../../core/models/order';

@Injectable({
  providedIn: 'root'
})
export class GetOrdersResolver implements Resolve<Order[]> {

  constructor(private orderService: OrdersService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Order[]> {
    return this.orderService.orders;
  }
}
