import {Injectable} from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {Observable, of} from 'rxjs';
import {Order} from '../../core/models/order';
import {OrdersService} from '../services/orders.service';

@Injectable({
  providedIn: 'root'
})
export class GetOrderResolver implements Resolve<Order> {

  constructor(private orderService: OrdersService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Order> {
    const orderId = route.paramMap.get('id');
    return this.orderService.getOrderById$(Number(orderId));
  }
}
