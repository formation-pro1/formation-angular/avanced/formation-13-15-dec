import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {VersionService} from '../../services/version.service';
import {AuthService} from '../../../shared/auth.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {

  public version!: number;

  public user: { login: string } = {login: ''};
  public userConnected$! :Observable<boolean>;

  constructor(private versionService: VersionService,
              private authService: AuthService) {
    this.versionService.numVersion$.subscribe((next) => this.version = next);
  }

  ngOnInit(): void {
    this.userConnected$ = this.authService.isLoggedIn$;
    this.authService.getCurrentUser$.subscribe(user => {
      this.user = {login: user?.login || 'default'}
    })
  }

  public logoutUser(): void {
    this.authService.logout();
  }
}
